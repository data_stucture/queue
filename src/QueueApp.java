public class QueueApp {
    public static void main(String[] args) {
        // Queue q = new Queue(200);
        // int num = 1;
        // while (!q.isFull()) q.insert(65160000 + num++);
        // while (!q.isEmpty()) System.out.println(q.remove());

        // Queue theQueue = new Queue(5);
        // theQueue.insert(10);
        // theQueue.insert(20);
        // theQueue.insert(30);
        // theQueue.insert(40);
        // theQueue.remove();
        // theQueue.remove();
        // theQueue.remove();
        // theQueue.insert(50);
        // theQueue.insert(60);
        // theQueue.insert(70);
        // theQueue.insert(80);
        // while (!theQueue.isEmpty()) {
        //     long n = theQueue.remove();
        //     System.out.print(n);
        //     System.out.print(" ");
        // }
        // System.out.println("");

        // QueueWithoutnItem theQueueWithoutnItem = new QueueWithoutnItem(5);
        // theQueueWithoutnItem.insert(10);
        // theQueueWithoutnItem.insert(20);
        // theQueueWithoutnItem.insert(30);
        // theQueueWithoutnItem.insert(40);
        // theQueueWithoutnItem.remove();
        // theQueueWithoutnItem.remove();
        // theQueueWithoutnItem.remove();
        // theQueueWithoutnItem.insert(50);
        // theQueueWithoutnItem.insert(60);
        // theQueueWithoutnItem.insert(70);
        // theQueueWithoutnItem.insert(80);
        // while (!theQueueWithoutnItem.isEmpty()) {
        //     long n = theQueueWithoutnItem.remove();
        //     System.out.print(n);
        //     System.out.print(" ");
        // }
        // System.out.println("");

        PriorityQ thePQ = new PriorityQ(5);
        thePQ.insert(30);
        thePQ.insert(50);
        thePQ.insert(10);
        thePQ.insert(40);
        thePQ.insert(20);
        while (!thePQ.isEmpty()) {
            long item = thePQ.remove();
            System.out.print(item + " ");
        }
        System.out.println("");
    }
}
